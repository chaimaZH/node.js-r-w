const fs = require("fs");

//create the directory if it does not exists
if (!fs.existsSync("./new")) {
  fs.mkdir("./new", (err) => {
    if (err) throw err;
    console.log("Directory created");
  });
}

//delete the directory if it exists
if (fs.existsSync("./new")) {
    fs.rmdir("./new", (err) => {
      if (err) throw err;
      console.log("Directory removed");
    });
  }