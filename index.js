// //the execution will happen asynchronous
// const fs = require("fs");
// const path = require("path");
// // fs.readFile("./files/starter.txt","utf8", (err, data) => {
// //   if (err) throw err;
// //   console.log(data)//output <Buffer 68 65 6c 6c 6f 20 77 6f 72 6c 64>
// //   console.log(data.toString()); // output hello world
// //   console.log(data)
// // });
// fs.readFile(
//   path.join(__dirname, "files", "starter.txt"),
//   "utf8",
//   (err, data) => {
//     if (err) throw err;
//     console.log(data);
//   }
// );
// console.log("hello ......."); //this is written in the console while node is getting the data from the file
// fs.writeFile(
//   path.join(__dirname, "files", "reply.txt"),
//   "nice to meet you ",
//   (err) => {
//     if (err) throw err;
//     console.log("write complete");
//     fs.appendFile(
//       path.join(__dirname, "files", "reply.txt"),
//       "\n\n Yes it is ",
//       (err) => {
//         if (err) throw err;
//         console.log("Append complete");

//         fs.rename(
//           path.join(__dirname, "files", "reply.txt"),
//           path.join(__dirname, "files", "Newreply.txt"),
//           (err) => {
//             if (err) throw err;
//             console.log("Rename complete");
//           }
//         );
//       }
//     );
//   }
// );

// fs.appendFile(
//   path.join(__dirname, "files", "test.txt"),
//   "testing text ",
//   (err) => {
//     if (err) throw err;
//     console.log("Append complete");
//   }
// ); //create file if it does not exist

// //exit on uncaught errors
// process.on("uncaughtException", (err) => {
//   console.error(`There was an uncaught error :${err} `);
//   process.exit(1);
// });

const fsPromises = require("fs").promises;
const path = require("path");

const fileOps = async () => {
  try {
    const data = await fsPromises.readFile(
      path.join(__dirname, "files", "starter.txt"),
      "utf8"
    );
    console.log(data);
    //delete a file
    await fsPromises.unlink(
        path.join(__dirname, "files", "starter.txt")
      );
    await fsPromises.writeFile(
      path.join(__dirname, "files", "promiseWrite.txt"),
      data
    );
    await fsPromises.appendFile(
      path.join(__dirname, "files", "promiseWrite.txt"),
      "\n\nNice to meet you."
    );
    await fsPromises.rename(
      path.join(__dirname, "files", "promiseWrite.txt"),
      path.join(__dirname, "files", "promiseComplete.txt")
    );
    const NewData = await fsPromises.readFile(
        path.join(__dirname, "files", "promiseComplete.txt"),
        "utf8"
      );
      console.log(NewData);
  } catch (err) {
    console.log(err);
  }
};

fileOps();
